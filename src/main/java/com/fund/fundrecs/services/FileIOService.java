package com.fund.fundrecs.services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.ParseException;
import java.util.Scanner;

public class FileIOService {

    /**
     * Writes a String of Transaction data to a file
     * 
     * @param nameOfFile
     * @param dataToBeWritten
     * @throws IOException
     */

    public static void writeToFile(String dataToBeWritten, String nameOfFile, boolean append) throws IOException {

        try (Writer writer = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(nameOfFile, append), "utf-8"))) {
            writer.write(dataToBeWritten);
            writer.close();
        }
    }

    /**
     * Looks for a file, name found in application.properties
     * Reads it and returns a List of Transactions or: ""
     * 
     * @param nameOfFile
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public static String getStringFromFile(String nameOfFile)
            throws IOException, ParseException {
        StringBuilder jsonStringBuilder = new StringBuilder();

        try {
            File data = new File(nameOfFile);
            System.out.println(data.getAbsolutePath());
            Scanner myReader = new Scanner(data);
            while (myReader.hasNextLine()) {
                jsonStringBuilder.append(myReader.nextLine());
            }
            myReader.close();
            if (jsonStringBuilder.toString().isEmpty()) {
                jsonStringBuilder.append("");
            }
        } catch (FileNotFoundException e) {
            // file not found
            jsonStringBuilder.append("");
        }

        return jsonStringBuilder.toString();
    }
}
