package com.fund.fundrecs.services;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fund.fundrecs.utils.ValidationHelper;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import model.Transaction;

public class TransactionServices {

    public TransactionServices() {
    }

    /**
     * Looks for a file, name found in application.properties
     * Reads it and returns a List of Transactions or: "[]"
     * 
     * @param transactionDocumentName
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public static List<Transaction> getTransactionData(String transactionDocumentName)
            throws IOException, ParseException {

        String transactionsFileAsString = FileIOService.getStringFromFile(transactionDocumentName);

        if (transactionsFileAsString == "") {
            transactionsFileAsString = "[]";
        }

        try {
            ObjectMapper mapper = new ObjectMapper();
            List<Transaction> storedData = mapper.readValue(transactionsFileAsString,
                    new TypeReference<List<Transaction>>() {
                    });
                    return storedData;
        } catch (Exception e) {
            return new ArrayList<Transaction>();
        }
    }

    /**
     * Compares the stored transactions with the user posted transaction.
     * If transaction type and date are the same, the transactions amount is sumed.
     * If there is not a comparsion in the transactions, the posted transaction
     * is added to the List to be saved to file
     * 
     * @param postedTransactions
     * @param currentTransactions
     * @return
     */
    public static List<Transaction> checkMatchAndUpdate(List<Transaction> postedTransactions,
            List<Transaction> currentTransactions, String loggerFileName) {

        // No file so return postedTransactions to be written as new file.
        if (currentTransactions.size() == 0) {
            return postedTransactions;
        }

        HashSet<Transaction> newEntries = new HashSet<Transaction>();
        List<Transaction> entriesToRemoveDueToEntryUpdate = new ArrayList<Transaction>();
        for (Transaction currentTransaction : currentTransactions) {
            for (Transaction postedTransaction : postedTransactions) {
                if (postedTransaction.getDate().equals(currentTransaction.getDate())
                        &&
                        postedTransaction.getType().equals(currentTransaction.getType())) {

                    try {
                        FileIOService.writeToFile(generateLog(postedTransaction, currentTransaction), loggerFileName, true);
                    } catch (IOException e) {

                        e.printStackTrace();
                    }
                    double sum = Double.parseDouble(postedTransaction.getAmount())
                            + Double.parseDouble(currentTransaction.getAmount());
                    currentTransaction.setAmount(Double.toString(Math.round(sum * 100.0) / 100.0));
                    entriesToRemoveDueToEntryUpdate.add(postedTransaction);
                } else {
                    newEntries.add(postedTransaction);
                }
            }
        }

        newEntries.removeAll(entriesToRemoveDueToEntryUpdate);

        if (newEntries.size() > 0) {
            List<Transaction> merge = new ArrayList<Transaction>();
            merge.addAll(currentTransactions);
            merge.addAll(newEntries);
            return merge;
        }
        return currentTransactions;
    }

    public static String generateLog(Transaction transactionA, Transaction transactionB){
        double sum = Double.parseDouble(transactionA.getAmount()) + Double.parseDouble(transactionB.getAmount());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String log = "Transaction with Date: " + transactionA.getDate() + " with Type: " + transactionA.getType() 
        + " and an Amount: " + transactionA.getAmount() + " equals a Transaction with with Amount of: " + transactionB.getAmount() 
        + " will now be one Transaction with the Amount of: " + sum + " on: " + timestamp + "\n";

        return log;
    }

    /**
     * Transactions are saved to file as a String of text.
     * This method generates a string in JSON format, from a List of Transactions.
     * 
     * @param transactions
     * @return
     */

    public static String generateString(List<Transaction> transactions) {
        StringBuilder buildTransactionJSONString = new StringBuilder();
        buildTransactionJSONString.append("[");

        for (Transaction transaction : transactions) {
            buildTransactionJSONString.append("\n\t{");
            buildTransactionJSONString.append("\n\t\t\"date\" : \"" + transaction.getDate() + "\",");
            buildTransactionJSONString.append("\n\t\t\"type\" : \"" + transaction.getType() + "\",");
            buildTransactionJSONString.append("\n\t\t\"amount\" : \"" + transaction.getAmount() + "\"");
            buildTransactionJSONString.append("\n\t},");
        }
        buildTransactionJSONString.append("\n]");

        // remove last comma
        StringBuffer buildTransactionJSONStringSB = new StringBuffer(buildTransactionJSONString);
        buildTransactionJSONStringSB.deleteCharAt(buildTransactionJSONStringSB.length() - 3);
        String transactionJSONString = buildTransactionJSONStringSB.toString();

        return transactionJSONString;
    }

    /**
     * Given a string of json formatted Transaction without the "amount"
     * will return it as a string with the "amount".
     * If the Transaction exists.
     *
     * @param userSearchTransaction
     * @param currentTransactions
     * @throws ParseException
     * @throws IOException
     * @throws org.json.simple.parser.ParseException
     */
    public static String findTransaction(String transactionSearchData, List<Transaction> currentTransactions)
            throws ParseException, IOException, org.json.simple.parser.ParseException {

        Transaction search = new Transaction();
        JSONParser parser = new JSONParser();
        JSONObject userPostedSearch = (JSONObject) parser.parse(transactionSearchData);

        try {
            search.setDate(userPostedSearch.get("date").toString());
            search.setType(userPostedSearch.get("type").toString());
        } catch (Exception e) {
            return null;
        }

        for (Transaction transaction : currentTransactions) {
            if (transaction.getDate().equals(search.getDate()) && transaction.getType().equals(search.getType())) {
                search.setAmount(transaction.getAmount());
            }
        }
        List<Transaction> forSringing = new ArrayList<Transaction>();
        forSringing.add(search);
        return TransactionServices.generateString(forSringing);

    }

    /**
     * 
     * Check data posted by the user
     * 
     * @param transactions
     * @return
     */
    public static boolean validateTransactionData(List<Transaction> transactions) {
        for (Transaction transaction : transactions) {
            if (transaction.getAmount() == null ||
                    transaction.getType() == null ||
                    transaction.getDate() == null ||
                    !ValidationHelper.isValidDate(transaction.getDate()) ||
                    !ValidationHelper.isNumeric(transaction.getAmount())) {
                return false;
            }
        }

        return true;
    }

    /**
     * If there are any transactions on the same date posted by the user in one request, this method
     * will return a list of a uniquely Dated transactions with the previcusly duplicated summed ninto one transaction
     * 
     * @param postedTransactions
     * @return
     */
    public static List<Transaction> dealWithSameDateTransactionData(List<Transaction> postedTransactions) {
        HashSet<String> checkList = new HashSet<>();
        for (int i = 0; i < postedTransactions.size(); i++) {
            for (int j = 0; j < postedTransactions.size(); j++) {
                if (i != j && postedTransactions.get(i).getDate().equals(postedTransactions.get(j).getDate())) {
                    if (i > j) {
                        checkList.add(i + "," + j);
                    } else {
                        checkList.add(j + "," + i);
                    }
                }
            }
        }

        ArrayList<Integer> entriesToRemove = new ArrayList<Integer>();

        for (String string : checkList) {
            ArrayList<String> common = new ArrayList<>(Arrays.asList(string.split(",")));
            Double tempAmount1 = Double
                    .parseDouble(postedTransactions.get(Integer.parseInt(common.get(0))).getAmount());
            Double tempAmount2 = Double
                    .parseDouble(postedTransactions.get(Integer.parseInt(common.get(1))).getAmount());
            Double summed = tempAmount1 + tempAmount2;

            postedTransactions.get(Integer.parseInt(common.get(0))).setAmount(summed.toString());
            entriesToRemove.add(Integer.parseInt(common.get(1)));
        }

        List<Transaction> cleanedTransactions = new ArrayList<Transaction>();

        boolean check = false;
        for (int i = 0; i < postedTransactions.size(); i++) {
            for (Integer postition : entriesToRemove) {
                if (i == postition) {
                    check = true;
                    break;
                }
            }
            if (check) {
                check = false;
            } else {
                cleanedTransactions.add(postedTransactions.get(i));
            }

        }

        return cleanedTransactions;
    }


    /**
     * Takes a list of transactions and sorts them by date
     * 
     * @param transactions
     * @return
     */
    public static List<Transaction> sortByDate(List<Transaction> transactions) {
        Collections.sort(transactions, (x, y) -> LocalDate
                .parse(x.getDate(), DateTimeFormatter.ofPattern("d-M-uuuu").withResolverStyle(ResolverStyle.STRICT))
                .compareTo(
                        LocalDate.parse(y.getDate(),
                                DateTimeFormatter.ofPattern("d-M-uuuu").withResolverStyle(ResolverStyle.STRICT))));
        return transactions;
    }

}
