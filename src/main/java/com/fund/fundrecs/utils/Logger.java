package com.fund.fundrecs.utils;
import org.slf4j.LoggerFactory;

import model.Transaction;

public class Logger {
    private static org.slf4j.Logger logger = LoggerFactory.getLogger(Logger.class);

    public void logger(){}
    
    public static void logTransaction(Transaction transaction) {
        System.out.println("Received new transaction: " + transaction.getDate() + " " + transaction.getType() + " " + transaction.getAmount());
        logger.info("Received new transaction: " + transaction.getDate() + " " + transaction.getType() + " " + transaction.getAmount());
    }

    public static void logTransactionSearch(String search) {
        System.out.println("Received new transaction Search: " + search);
        logger.info("Received new transaction: " + search);
    }

}
