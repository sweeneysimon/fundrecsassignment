package com.fund.fundrecs.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;

public class ValidationHelper {

    /**
     * Helper method for validateTransactionData
     * 
     * @param transactionDate
     * @return
     */
    public static boolean isValidDate(String transactionDate) {
        try {
            LocalDate.parse(transactionDate,
                    DateTimeFormatter.ofPattern("d-M-uuuu")
                            .withResolverStyle(ResolverStyle.STRICT));

        } catch (DateTimeParseException e) {
            return false;
        }

        return true;
    }

    /**
     * Helper method for validateTransactionData
     * 
     * @param number
     * @return
     */
    public static boolean isNumeric(String number) {
        if (number == null || number.equals("")) {
            return false;
        }

        try {
            Double.parseDouble(number);
            return true;
        } catch (NumberFormatException e) {
        }
        return false;
    }
    
}
