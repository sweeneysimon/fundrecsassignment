package com.fund.fundrecs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FundrecApplication {

	public static void main(String[] args) {
		SpringApplication.run(FundrecApplication.class, args);
	}

}