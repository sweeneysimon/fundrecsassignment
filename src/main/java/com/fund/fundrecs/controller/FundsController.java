package com.fund.fundrecs.controller;

import java.io.IOException;
import java.util.List;

import com.fund.fundrecs.services.FileIOService;
import com.fund.fundrecs.services.TransactionServices;
import com.fund.fundrecs.utils.Logger;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import model.Transaction;

@RestController
public class FundsController {

    @Value("${fileName}")
    private String transactionDocumentName;

    @Value("${loggerFile}")
    private String loggerFileName;


    @PostMapping(value = "/addTransaction")
    public ResponseEntity<String> transaction(@RequestBody List<Transaction> transactions)
            throws IOException, ParseException, java.text.ParseException {

        if (!TransactionServices.validateTransactionData(transactions)) {
            return new ResponseEntity<>("Transaction data is not valid.", HttpStatus.BAD_REQUEST);
        }

        for (Transaction transaction : transactions) {
            Logger.logTransaction(transaction);
        }

        List<Transaction> uniqueTransactions = TransactionServices.dealWithSameDateTransactionData(transactions);

        List<Transaction> currentTransaction = TransactionServices.getTransactionData(transactionDocumentName);

        currentTransaction = TransactionServices.checkMatchAndUpdate(uniqueTransactions, currentTransaction, loggerFileName);

        currentTransaction = TransactionServices.sortByDate(currentTransaction);

        String transactionString = TransactionServices.generateString(currentTransaction);

        FileIOService.writeToFile(transactionString, transactionDocumentName, false);

        return new ResponseEntity<>("Transaction has been added", HttpStatus.OK);
    }

    @GetMapping(value = "/getAmount")
    public ResponseEntity<String> getTransaction(@RequestBody String transactionSearchData)
            throws IOException, ParseException, java.text.ParseException {

        Logger.logTransactionSearch(transactionSearchData);

        List<Transaction> currentTransactions = TransactionServices.getTransactionData(transactionDocumentName);

        String transaction = TransactionServices.findTransaction(transactionSearchData, currentTransactions);

        if (transaction == null) {
            return new ResponseEntity<>("Data sent did not resemble a Transaction", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(transaction, HttpStatus.OK);
    }


    @GetMapping(value = "/")
    public ResponseEntity<String> home()
            throws IOException, ParseException, java.text.ParseException {

        return new ResponseEntity<>("Go to https://bitbucket.org/sweeneysimon/fundrecsassignment/src/master/ for documentation for this api.", HttpStatus.OK);
    }
}
