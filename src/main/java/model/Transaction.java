package model;

public class Transaction {
    private String date;
    private String type;
    private String amount;

    public Transaction() {
    }

    public Transaction(String date, String type, String amount) {
        this.setDate(date);
        this.setType(type);
        this.setAmount(amount);
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
