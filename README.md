# README #	

### What is this repository for? ###

This is a 'Skill Demo' Assignment given by FundRecs to demonstrate abilities with backend Java.

### Deployment ###

This App is deployed at Heroku  

URL: https://whispering-hamlet-67914.herokuapp.com

### Endpoints ###

 This App has two endpoints a GET and a POST  
 
* GET /getAmount 
* POST /addTransaction

### Example Requests ###
#### Example POST request ####

````
curl --location --request POST 'https://whispering-hamlet-67914.herokuapp.com/addTransaction' \
--header 'Content-Type: application/json' \
--data-raw '[
   {
       "date": "04-09-2020",
       "type": "credit",
       "amount": "1"
   },
   {
       "date": "04-09-2019",
       "type": "credit",
       "amount": "2"
   }
]'
````

````
##### Responce to request : Transaction has been added #####
````
  
#### Example GET request ####
````
curl --location --request GET 'https://whispering-hamlet-67914.herokuapp.com/getAmount' \
--header 'Content-Type: application/json' \
--data-raw '{
    "date": "04-09-2020",
    "type": "credit"
}'
````
##### Responce to request : [
	{
		"date" : "04-09-2020",
		"type" : "credit",
		"amount" : "1"
	} ]
	
### Running Locally ###

App runs locally on port 8080  
Endpoints would be:  

* localhost:8080/addTransaction
* localhost:8080/getAmount

### Assignment Specs Given ###

 Programming Assignment:  
 
 Design a web service that accepts transactions data in JSON format and saves them to file.  
 A transaction is uniquely identified by date and type. If such a transaction already exists  
 in a file, then sum the amounts of 2 transactions but save only one of those.  
 When this Event happens (transaction with date and type already exists and we sum the amounts), we need to send a message  
 to external system. For this assignment, external system can simply mean writing event log to another file.  
 Data in file can be saved in sorted ordered by dates.  
 Sample Input Request:  
 [
   
   {  
       "date": "11-12-2018",  
       "type": "credit",  
       "amount": "9898.36"  
   },  
   {  
       "date": "11-12-2019",  
       "type": "credit",  
       "amount": "98.36"  
   }  
 ]  
 
 We also need to get transaction based on date and type.  
 Sample Get Request  
                {  
       "date": "11-12-2019",  
       "type": "credit"  
                }  
 Sample Get Response  
                {  
       "date": "11-12-2019",  
       "type": "credit",  
       "amount": "98.36"  
   }  
 Since number of transactions can grow, retrieving data needs to be efficient.  
 You only need to design one GET and one POST Api.  
 It is desirable, but not required, if you can also deploy code to public cloud of your choice and  
 share the deployment scripts if any.   
 If assignment taking long, please submit what is completed and rest of  
 the steps can be explained via email.  
 If possible please use one of the following choices for solution:  
 Spring Boot or Grails, And Java or Groovy.  

